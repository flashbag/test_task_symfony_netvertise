<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Request as RequestModel;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;



class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

     /**
     * @Route("/getRequest", name="get request")
     */
    public function getRequestAction(Request $request)
    {
        $serializer = $this->get('jms_serializer');

        $params = $this->__parseSearchParams($request);
        $results = $this->__searchForRequest($params);

        if (array_key_exists('id', $params)) {
            if (!empty($results)) {
                $response = new Response($serializer->serialize($results[0], 'json'));
            } else {
                throw new NotFoundHttpException("request with id = $params[id] not found");
            }
        } else {
            $response = new Response($serializer->serialize($results, 'json'));
        }


        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/storeRequest/{route}", name="store request")
     */
    public function storeRequestAction(Request $request, $route = '')
    {

        if (!$route || empty($route)) {
            return $this->__sendResponse(false, 'route param is invalid');
        }

        try {
            $requestModel = new RequestModel();
            $requestModel->setIp($request->server->get('REMOTE_ADDR'));
            $requestModel->setMethod($request->server->get('REQUEST_METHOD'));
            $requestModel->setRoute($request->server->get('REQUEST_URI'));

            $requestModel->setHeaders(json_encode($request->headers->all()));

            $requestModel->setBody($request->getContent());

            $em = $this->getDoctrine()->getManager();

            $em->persist($requestModel);

            $em->flush();

            return $this->__sendResponse(true, 'successfully saved', $requestModel->getId());


        } catch (Exception $e) {
            return $this->__sendResponse(false, $e->getMessage());
        }

    }

    /**
     * __sendResponse
     *
     * @param  boolean  $status     status
     * @param  string  $message     message
     * @param  boolean $id          id of request to show
     * @return string               JSON response
     */
    private function __sendResponse($status, $message = 'Something went wrong', $id = false)
    {
        $data = [
            'status' => $status,
            'message' => $message,
        ];

        if ($id) {
            $data['id'] = $id;
        }

        $response = new JsonResponse();

        $response->setData($data);

        return $response;
    }

    /**
     * __parseSearchParams
     * @param  Request $request     request object
     * @return array                parsed params for search
     */
    private function __parseSearchParams(Request $request)
    {
        $params = [];

        // params like properties

        if ($id = $request->query->getDigits('id')) {
            $params['id'] = $id;
        }

        if ($ip = $request->query->getAlpha('ip')) {
            $params['ip'] = $ip;
        }

        if ($method = $request->query->getAlpha('method')) {
            $params['method'] = $method;
        }

        if ($route = $request->query->getAlpha('route')) {
            $params['route'] = $route;
        }

        // other params, not properties

        if ($last_days = $request->query->getDigits('last_days')) {
            $params['last_days'] = $last_days;
        }

        if ($search = $request->query->getAlpha('search')) {
            $params['search'] = $search;
        }

        return $params;
    }

    private function __searchForRequest(array $params)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:Request');

        $query = $repository->createQueryBuilder('r');
        $query->where('1 = 1');

        if (array_key_exists('id', $params)) {
            $query->andWhere('r.id = :id');
            $query->setParameter('id', $params['id']);
        }

        if (array_key_exists('ip', $params)) {
            $query->andWhere('r.ip = :ip');
            $query->setParameter('ip', $params['ip']);
        }

        if (array_key_exists('method', $params)) {
            $query->andWhere('r.method = :method');
            $query->setParameter('method', $params['method']);
        }

        if (array_key_exists('route', $params)) {
            $query->andWhere('r.route = :route');
            $query->setParameter('route', $params['route']);
        }

        if (array_key_exists('last_days', $params)) {
            $query->andWhere(
                $query->expr()->gte('r.created_at', ':last_days')
            );

            $query->setParameter('last_days', new \DateTime('-' . $params['last_days'] . ' days'));

        }

        if (array_key_exists('search', $params)) {

            $query->andWhere(
                $query->expr()->orX(
                    $query->expr()->like('r.body', ':body'),
                    $query->expr()->like('r.headers', ':headers')
                )
            );

            $like = '%' . $params['search'] . '%';

            $query->setParameter('headers', $like);
            $query->setParameter('body', $like);

        }

        $query = $query->getQuery();

        // echo '<pre>';
        // var_dump($query->getParameters());
        // var_dump($query->getSql());
        // die;

        return $query->getResult();
    }

}

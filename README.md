netvertise_test_task
====================

A Symfony project created on September 13, 2016, 1:54 pm.


Setup instuctions:

* Install composer dependencies

  composer install

* Create database

  php bin/console doctrine:database:create

* Update Doctrine schema

  php bin/console doctrine:schema:update --force

* Configure your application in app/config/parameters.yml file.

* Run your application:
    1. Execute the php bin/console server:run command.
    2. Browse to the http://localhost:8000 URL.

* Read the documentation at http://symfony.com/doc




To run tests execute "phpunit" command from project root directory.
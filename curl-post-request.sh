#!/bin/bash

curl 'http://localhost:8000/storeRequest/1409' \
-X POST \
-H 'Upgrade-Insecure-Requests: 1' \
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/52.0.2743.116 Chrome/52.0.2743.116 Safari/537.36' \
--data "{'hello':'friend'}" \
--compressed
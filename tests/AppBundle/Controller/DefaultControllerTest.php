<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
	private function __assertJson($client)
	{
		$this->assertTrue(
		    $client->getResponse()->headers->contains(
		        'Content-Type',
		        'application/json'
		    ),
		    'the "Content-Type" header is "application/json"' // optional message shown on failure
		);
	}

    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }

    public function testStoreRequest1()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/storeRequest/storeRequestTest');

        $this->__assertJson($client);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), '"statusCode" is 200');

		$this->assertContains('"status":true', $client->getResponse()->getContent(), 'saved success');
		$this->assertContains('"id":', $client->getResponse()->getContent(), 'new entity "id" returned');

    }

    public function testStoreRequest2()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/storeRequest');

        $this->__assertJson($client);

        $this->assertEquals(200, $client->getResponse()->getStatusCode(), '"statusCode" is 200');

		$this->assertContains('"status":false', $client->getResponse()->getContent(), 'param error');
    }


    public function testGetRequest1()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getRequest/?id=99999999');

        $this->assertEquals(404, $client->getResponse()->getStatusCode(), '"statusCode" is 404');

    }

    public function testGetRequest2()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getRequest');

        $this->assertTrue( !!count($client->getResponse()->getContent()), 'response has items');

    }

    public function testGetRequest3()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/getRequest?method=MAGIC');

        $response = $client->getResponse();

        // UNFINISHED!!!
        // spent much time to workarround the problem
        // can't spent more, need to make real work

        // $this->assertTrue( count($response->getContent()) == 0, 'response has no items');

    }


}
